<?php

/**
 * @file
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */

/**
 * 
 */
function file_preview_imagemagick_convert_exec($command_args, &$output, &$errors, $convert_path = NULL) {
  if (!isset($convert_path)) {
    $convert_path = variable_get('file_preview_imagemagick_convert_path', '');
  }

  if ($errors = file_preview_imagemagick_check_path($convert_path)) {
    watchdog('file_preview', '<pre>@dump</pre>', array('@dump' => print_r($errors, TRUE)), WATCHDOG_DEBUG);
    return FALSE;
  }

  if (preg_match('`(?:Win32|Win64|IIS)`', $_SERVER['SERVER_SOFTWARE'])) {
    // Use Window's start command to avoid the "black window" from showing up:
    // http://us3.php.net/manual/en/function.exec.php#56599
    // Use /D to run the command from PHP's current working directory so the
    // file paths don't have to be absolute.
    $convert_path = 'start "window title" /D'. escapeshellarg(getcwd()) .' /B '. escapeshellarg($convert_path);
  }

  $descriptors = array(
    0 => array('pipe', 'r'), // stdin
    1 => array('pipe', 'w'), // stdout
    2 => array('pipe', 'w')  // stderr
  );
  if ($h = proc_open($convert_path .' '. $command_args, $descriptors, $pipes, $_SERVER['DOCUMENT_ROOT'])) {
    $output = '';
    while (!feof($pipes[1])) {
      $output .= fgets($pipes[1]);
    }

    $errors = '';
    while (!feof($pipes[2])) {
      $errors .= fgets($pipes[2]);
    }

    if ($errors) {
      watchdog('file_preview', '<pre>@dump</pre>', array('@dump' => print_r($errors, TRUE)), WATCHDOG_DEBUG);
      return FALSE;
    }

    fclose($pipes[0]);
    fclose($pipes[1]);
    fclose($pipes[2]);
    return proc_close($h);
  }

  return FALSE;
}

/**
 * 
 */
function file_preview_imagemagick_check_path($path) {
  $errors = array();

  if (!$path || !is_file($path)) {
    $errors[] = 'The specified ImageMagick path does not exist.';
  }
  if (!$errors && !is_executable($path)) {
    $errors[] = 'The specified ImageMagick path is not executable.';
  }
  if ($errors && ini_get('open_basedir')) {
    $errors[] = 'PHP\'s open_basedir security restriction is set which may be interfering with attempts to locate ImageMagick.';
  }

  return $errors;
}
